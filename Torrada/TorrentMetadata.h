#ifndef TORRENT_METADATA_H_INCLUDED
#define TORRENT_METADATA_H_INCLUDED

#import <Foundation/Foundation.h>

@interface TorrentMetadata : NSObject
@property (nullable, nonatomic) NSData *data;
@property (nullable, nonatomic) NSString *name;
@property (nullable, nonatomic) NSString *infoHash;

+ (TorrentMetadata * _Nonnull)create:(NSString * _Nonnull)name
                             tracker:(NSURL * _Nullable)trackerURL
                               files:(NSArray<NSString *> *_Nullable)files;

- (NSString * _Nonnull)save:(NSString * _Nonnull)path;

@end

#endif

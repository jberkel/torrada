#ifndef TORRENT_DESCRIPTOR_H_INCLUDED
#define TORRENT_DESCRIPTOR_H_INCLUDED

#import <Foundation/Foundation.h>
#import "URLSeed.h"

@interface TorrentDescriptor : NSObject<NSCopying>
// 20-byte SHA1 hash in String form, required
// e.g. b126c361ef5c1fdd20f1e0012b4633903a10f654
@property (nonnull, readonly, nonatomic) NSString *infoHash;
// the location of the .torrent file
@property (nullable, nonatomic, readonly) NSURL *url;
// optional web seed (http://bittorrent.org/beps/bep_0019.html)
@property (nullable, readonly, nonatomic) URLSeed *urlSeed;

- (nonnull instancetype)initWithUrl:(nullable NSURL *)url
                           infoHash:(nonnull NSString *)infoHash
                            urlSeed:(nullable URLSeed *)urlSeed;
@end

#endif

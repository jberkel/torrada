#import <Foundation/Foundation.h>
#if TARGET_OS_IPHONE
#import <UIKIT/UIKit.h>
#endif

#import "Torrada.h"
#import "Torrent-Private.hpp"
#import "TorrentMetadata-Private.hpp"
#import "connected_via_ipv6.h"
#import "system_proxy.h"
#import "TorradaSettings.h"

#pragma clang diagnostic push
#pragma clang diagnostic warning "-Wshorten-64-to-32"
#import <libtorrent/version.hpp>
#import <libtorrent/create_torrent.hpp>
#import <libtorrent/session.hpp>
#import <libtorrent/alert_types.hpp>
#import <libtorrent/torrent_info.hpp>
#import <libtorrent/torrent.hpp>
#import <libtorrent/extensions.hpp>
#import <libtorrent/ip_filter.hpp>
#pragma clang diagnostic pop

static NSError *translateError(libtorrent::error_code error);
static NSString *torradaVersion();

namespace lt = libtorrent;

const char *TorradaConfigSystemProxy(void) {
    return systemProxy().absoluteString.UTF8String;
}

@interface TorradaSession () {
    lt::session *_torrentSession;
    BOOL _quit;
    BOOL _started;
    dispatch_queue_t _runLoopQueue;
    // Descriptors, keyed by info hash
    NSMutableDictionary<NSString *, TorrentDescriptor *> *_torrentDescriptors;
#if TARGET_OS_IPHONE
    // background task identifier, keyed by descriptor
    NSMutableDictionary<TorrentDescriptor *, NSNumber *> *_backgroundTasks;
#endif
}
@end

@implementation TorradaSession

-(instancetype)initWithPath:(NSString *)path
{
    return [self initWithPath:path config:TorradaConfig {
        .debugEnabled =  NO,
        .removeTorrentsAfterDownload = YES,
        .maxTrackerRetries = 3,
        .proxy = TorradaConfigSystemProxy()
    }];
}

-(instancetype)initWithPath:(NSString *)path
                     config:(TorradaConfig)config
{
    if (self = [super init]) {
        NSParameterAssert(path);
        _path = path;
        _config = config;
#if DEBUG
        _logger = ^(NSString *format, va_list list) {
            NSLogv(format, list);
        };
#else
        _logger = nil;
#endif
        _torrentSession = new lt::session(defaultSettings(config, torradaVersion()), /* flags */ 0);
        _torrentSession->add_extension(create_plugin);
        {
            // avoid connecting to other Torrada clients
            lt::port_filter filter;
            std::vector<std::pair<uint16_t, uint16_t>> ranges({
                std::make_pair(0,     79),
                std::make_pair(81,   442),
                std::make_pair(444, 1024)
            });
            if (config.listenPort > 0) {
                ranges.push_back(std::make_pair(config.listenPort, std::min(config.listenPort + 10, 65535)));
            }
            for (auto i = ranges.begin(); i != ranges.end(); i++) {
                filter.add_rule(i->first, i->second, lt::port_filter::blocked);
            }
            _torrentSession->set_port_filter(filter);
        }
#if TARGET_OS_IPHONE
        _backgroundTasks = [[NSMutableDictionary alloc] init];
#endif
        _torrentDescriptors = [[NSMutableDictionary alloc] init];
        _started = NO;
        _quit = NO;
        _runLoopQueue = dispatch_queue_create("libtorrent-runloop", DISPATCH_QUEUE_SERIAL);
        [self log:@"%@ initialized with path: %@", torradaVersion(), self.path];
        if (config.proxy) {
            [self log:@"using proxy: %s", config.proxy];
        }
        return self;
    } else {
        return nil;
    }
}

- (BOOL)addTorrent:(TorrentDescriptor *)torrentDescriptor
{
    if (_torrentSession == nullptr) return NO;

    for (Torrent *torrent in [self torrents]) {
        if ([torrent.url isEqual:torrentDescriptor.url]) {
            // torrent is already part of the session
            return NO;
        }
    }

    lt::add_torrent_params params;

    params.save_path = self.path.UTF8String;
    params.storage_mode = lt::storage_mode_sparse;

    NSString *cached = [self cachedTorrent:torrentDescriptor.url];
    if (cached) {
        [self log:@"using cached torrent %@", cached];
        NSData *data = [NSData dataWithContentsOfFile:cached];
        boost::shared_ptr<lt::torrent_info> ti(new lt::torrent_info((const char *)data.bytes, (int)data.length, 0));
        params.ti = ti;
    } else {
        params.url = [torrentDescriptor.url absoluteString].UTF8String;
    }

    NSURL *webSeedURL = [self webSeedURL:torrentDescriptor];
    if (webSeedURL) {
        [self log:@"using URL seed: %@", torrentDescriptor.urlSeed];
        std::vector<std::string> seeds({(torrentDescriptor.urlSeed.url.absoluteString.UTF8String)});
        params.url_seeds = seeds;
    }

    {
        const char *input = torrentDescriptor.infoHash.UTF8String;
        char out[20];
        bool ret = lt::from_hex(input, 20, out);
        NSAssert(ret, @"error creating hash");
        params.info_hash = lt::sha1_hash(out);
        _torrentDescriptors[torrentDescriptor.infoHash] = torrentDescriptor;
    }

    NSString *resume = [self resumeFile:torrentDescriptor.url];
    if (resume) {
        [self log:@"using fast resume data %@", resume];
        NSData *data = [NSData dataWithContentsOfFile:resume];
        params.resume_data = std::vector<char>((const char *)data.bytes, (const char *)data.bytes+data.length);
    }
    [self startBackgroundTask: torrentDescriptor];
    _torrentSession->async_add_torrent(params);
    return YES;
}

- (nullable Torrent *)removeTorrent:(TorrentDescriptor *)torrentDescriptor
{
    lt::torrent_handle handle = [self findHandle: torrentDescriptor];
    if (handle.is_valid()) {
        Torrent *torrent = [Torrent fromHandle:handle];
        _torrentSession->remove_torrent(handle);
        return torrent;
    } else {
        return nil;
    }
}

- (void)runLoop
{
    if (!_started) {
        _started = YES;
        [self internalRun];
    }
}

- (BOOL)isClosed
{
    return !_started || _torrentSession == nullptr;
}

-(void)close
{
    if (_started) {
        _quit = YES;
    } else {
        delete _torrentSession;
        _torrentSession = nullptr;
    }
}

- (NSArray<Torrent *> *)torrents
{
    std::vector<lt::torrent_handle> torrents = _torrentSession->get_torrents();
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:torrents.size()];
    for (auto &handle : torrents) {
        [array addObject:[Torrent fromHandle:handle]];
    }
    return array;
}

#pragma mark private methods

- (void)log:(NSString *)format, ...
{
    va_list argp;
    va_start(argp, format);
    if (self.logger) {
        self.logger(format, argp);
    }
    va_end(argp);
}

- (void)processTick
{
    _torrentSession->post_torrent_updates();
#if POST_SESSION_STATS
    _torrentSession->post_session_stats();
#endif
    std::vector<lt::alert*> alerts;
    _torrentSession->pop_alerts(&alerts);
    for (auto &alert : alerts) {
        try {
            if (![self handle:alert] && self.config.debugEnabled) {
                [self log:@"%s: %s", alert->what(), alert->message().c_str()];
            }
        } catch(std::exception &e) {}
    }
    alerts.clear();
}

- (void)internalRun
{
    dispatch_async(_runLoopQueue, ^{
        if (_quit) {
            [self log:@"shutting down Torrada session..."];
            delete _torrentSession;
            _torrentSession = nullptr;
            [self send:[self sessionClosedNotification]];
            [self log: @"done"];
        } else {
            [self processTick];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), _runLoopQueue, ^{
                [self internalRun];
            });
        }
    });
}

- (nullable NSURL *) webSeedURL:(TorrentDescriptor *)torrentDescriptor
{
    if (torrentDescriptor.urlSeed &&
        (self.config.proxy   // always use a web seed when a proxy is set
        ||
        ((torrentDescriptor.urlSeed.mode == URLSeedModeUseAlways) ||
        (torrentDescriptor.urlSeed.mode == URLSeedModeOnlyOnIPv6 && connected_via_ipv6())))) {
        return torrentDescriptor.urlSeed.url;
    } else {
        return nil;
    }
}

- (nullable NSString *)cachedTorrent:(NSURL *)url
{
    NSString *name = [self nameForUrl: url];
    NSString *file = [[self.path stringByAppendingPathComponent:name] stringByAppendingPathExtension:@"torrent"];
    if ([NSFileManager.defaultManager fileExistsAtPath:file]) {
        return file;
    } else {
        return nil;
    }
}

- (nullable NSString *)resumeFile:(NSURL *)url
{
    NSString *name = [self nameForUrl: url];
    NSString *path = [[self.path stringByAppendingPathComponent:name] stringByAppendingPathExtension:@"resume"];
    if ([NSFileManager.defaultManager fileExistsAtPath:path]) {
        return path;
    } else {
        return nil;
    }
}

#pragma mark name / hash

- (NSString *)nameForUrl:(NSURL *)url
{
    return [[url lastPathComponent] stringByReplacingOccurrencesOfString:@"?torrent" withString:@""];
}

- (lt::torrent_handle) findHandle:(TorrentDescriptor *)descriptor
{
    if (_torrentSession == nullptr || descriptor == NULL) return lt::torrent_handle();

    std::vector<lt::torrent_handle> torrents = _torrentSession->get_torrents();

    for (auto &handle : torrents) {
        if (descriptor.infoHash) {
            NSString *hex = [NSString stringWithUTF8String:lt::to_hex(handle.info_hash().to_string()).c_str()];
            if ([descriptor.infoHash isEqualToString:hex]) {
                return handle;
            }
        }

        lt::torrent *torrent = handle.native_handle().get();
        if (torrent) {
            NSString *torrentURL = [NSString stringWithUTF8String:torrent->url().c_str()];
            if ([torrentURL isEqualToString:descriptor.url.absoluteString]) {
                return handle;
            }
        }
    }
    return lt::torrent_handle();
}

#pragma mark notifications
- (NSNotification *) torrentAddedNotification:(Torrent *)torrent
{
    return [NSNotification notificationWithName:@TORRADA_ADDED
                                         object:nil
                                       userInfo:@{ @"torrent": torrent }];
}

- (NSNotification *) torrentProgressNotification:(Torrent *)torrent
{
    return [NSNotification notificationWithName:@TORRADA_PROGRESS
                                         object:nil
                                       userInfo:@{ @"torrent": torrent }];
}

- (NSNotification *) torrentFinishedNotification:(Torrent *)torrent
{
    return [NSNotification notificationWithName:@TORRADA_FINISHED
                                         object:nil
                                       userInfo:@{ @"torrent": torrent }];
}

- (NSNotification *) torrentRemovedNotification:(const lt::torrent_removed_alert *)removedAlert
{
    // at this point the torrent handle can be invalid, must use info hash
    NSString *hash = [TorrentMetadata infoHash:removedAlert->info_hash];
    NSURL *url = _torrentDescriptors[hash].url;
    NSMutableDictionary *userInfo = @{ @"info_hash": hash }.mutableCopy;
    if (url) {
        userInfo[@"url"] = url;
    }
    return [NSNotification notificationWithName:@TORRADA_REMOVED
                                         object:nil
                                       userInfo:userInfo];
}

- (NSNotification *) torrentErrorNotification:(Torrent *)torrent
{
    return [NSNotification notificationWithName:@TORRADA_ERROR
                                         object:nil
                                       userInfo:@{ @"torrent": torrent }];
}

- (NSNotification *) sessionClosedNotification
{
    return [NSNotification notificationWithName:@TORRADA_SESSION_CLOSED
                                         object:nil
                                       userInfo:nil];
}

- (void) send:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotification:notification];
    });
}

- (BOOL) hasActiveTorrent
{
    std::vector<lt::torrent_handle> torrents = _torrentSession->get_torrents();
    for (auto &handle : torrents) {
        if (handle.status().state != lt::torrent_status::finished &&
            handle.status().state != lt::torrent_status::seeding) {
            return YES;
        }
    }
    return NO;
}

#pragma resume data handling

+ (void)saveResumeData:(boost::shared_ptr<lt::entry>)resumeDataEntry
                handle:(lt::torrent_handle) handle
                    to:(NSString *)path
{
    if (!handle.is_valid()) return;
    NSAssert(resumeDataEntry, @"no resume data");

    std::vector<char> out;
    bencode(std::back_inserter(out), *resumeDataEntry);
    NSString *name = [Torrent name:handle];
    NSString *target = [[path stringByAppendingPathComponent:name] stringByAppendingPathExtension:@"resume"];

    NSData *resumeData = [NSData dataWithBytes:&out[0] length:out.size()];

    [resumeData writeToFile:target atomically:YES];
}

#pragma mark alert handling

- (BOOL)handle:(const lt::alert *)alert
{
    using namespace libtorrent;

    if (const state_update_alert *stateUpdateAlert = alert_cast<state_update_alert>(alert)) {
        return [self handleStateUpdateAlert:stateUpdateAlert];
    } else if (const add_torrent_alert *addedAlert = alert_cast<add_torrent_alert>(alert)) {
        if (addedAlert->error) {
            Torrent *torrent = [Torrent fromAddTorrentParams:addedAlert->params];
            torrent.error = translateError(addedAlert->error);
            [self onTorrentAddFailed:torrent];
        } else {
            [self onTorrentAdded:[Torrent fromHandle:addedAlert->handle]];
        }
        return YES;
    } else if (const torrent_paused_alert *pausedAlert = alert_cast<torrent_paused_alert>(alert)) {
        [self onTorrentPaused:[Torrent fromHandle:pausedAlert->handle]];
        return YES;
    } else if (const torrent_finished_alert *finishedAlert = alert_cast<torrent_finished_alert>(alert)) {
        [self onTorrentFinished:[Torrent fromHandle:finishedAlert->handle] handle: finishedAlert->handle];
        return YES;
    } else if (const torrent_removed_alert *removedAlert = alert_cast<torrent_removed_alert>(alert)) {
        [self onTorrentRemoved: removedAlert];
        return YES;
    } else if (const torrent_error_alert *errorAlert = alert_cast<torrent_error_alert>(alert)) {
        [self onTorrentError:[Torrent fromHandle:errorAlert->handle] alert: errorAlert];
        return NO; // always log errors
    } else if (const tracker_error_alert *trackerErrorAlert = alert_cast<tracker_error_alert>(alert)) {
        return [self handleTrackerError:trackerErrorAlert];
    } else if (const metadata_received_alert *metadataReceivedAlert = alert_cast<metadata_received_alert>(alert)) {
        [[TorrentMetadata metadataFor:metadataReceivedAlert->handle] save:self.path];
        return YES;
    } else if (const save_resume_data_alert *resumeDataAlert = alert_cast<save_resume_data_alert>(alert)) {
        [self.class saveResumeData:resumeDataAlert->resume_data handle:resumeDataAlert->handle to:self.path];
        if (self.config.removeTorrentsAfterDownload) {
            _torrentSession->remove_torrent(resumeDataAlert->handle);
        }
        return YES;
    }
    return NO;
}

- (BOOL)handleStateUpdateAlert:(const lt::state_update_alert *)stateUpdateAlert
{
    BOOL handled = NO;
    for (auto &torrent_status : stateUpdateAlert->status) {
        if (torrent_status.state == lt::torrent_status::downloading &&
            torrent_status.total_payload_download == 0) {
            // don't notify about download states without real progress
            continue;
        }
        Torrent *torrent = [Torrent fromHandle:torrent_status.handle status:torrent_status];
        [self send:[self torrentProgressNotification:torrent]];
        handled = YES;
    }
    return stateUpdateAlert->status.empty() || handled;
}

- (BOOL)maybeFallbackToUrlSeed:(lt::torrent_handle)handle
{
    if (!handle.url_seeds().empty()) return NO;

    const TorrentDescriptor *descriptor = _torrentDescriptors[[TorrentMetadata infoHash:handle.info_hash()]];
    if (descriptor && descriptor.urlSeed) {
        [self log: @"adding url seed %@", descriptor.urlSeed];
        handle.add_url_seed(descriptor.urlSeed.url.absoluteString.UTF8String);
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)handleTrackerError:(const lt::tracker_error_alert *)trackerErrorAlert {
    [self log:@"tracker failure %d/%d (%s), retriable: %@",
        trackerErrorAlert->times_in_row,
        self.config.maxTrackerRetries,
        trackerErrorAlert->error.message().c_str(),
        [self retriableError:trackerErrorAlert->error] ? @"yes" : @"no"
    ];

    lt::torrent_handle handle = trackerErrorAlert->handle;

    if (trackerErrorAlert->times_in_row == 2) {
        return [self maybeFallbackToUrlSeed:handle];
    } else if (self.config.maxTrackerRetries > 0 &&
            trackerErrorAlert->times_in_row >= self.config.maxTrackerRetries &&
            handle.url_seeds().empty())
    {
        [self transitionHandle:handle toErrorState:trackerErrorAlert->error];
    }
    return NO;
}

- (void) onTorrentAdded:(Torrent *) torrent
{
    [self send: [self torrentAddedNotification:torrent]];
}

- (void) onTorrentAddFailed:(Torrent *) torrent
{
    NSAssert(torrent.error, @"torrent has no error ser");
    [self log:@"adding torrent `%@` failed: %@", (torrent.name ? torrent.name : torrent.url), torrent.error];
    [self send:[self torrentErrorNotification:torrent]];
}

- (void)transitionHandle:(lt::torrent_handle)handle toErrorState:(lt::error_code) error
{
    NSAssert(handle.is_valid(), @"invalid handle");
    if (handle.status().errc) {
        return;
    }
    [self log:@"transitionHandle:%@ toErrorState:%s",
        [TorrentMetadata infoHash:handle.info_hash()],
        error.message().c_str()];
    handle.native_handle()->set_error(error, lt::torrent_status::error_file_url);
    handle.pause();
}

- (void) onTorrentPaused:(Torrent *)torrent
{
    [self send:[self torrentProgressNotification: torrent]];
}

- (void) onTorrentFinished:(Torrent *)torrent handle:(lt::torrent_handle) handle
{
    [self send:[self torrentFinishedNotification: torrent]];
    [self endBackgroundTask:_torrentDescriptors[torrent.infoHash]];
    handle.save_resume_data();
}

- (void) onTorrentError:(Torrent *)torrent alert:(const lt::torrent_error_alert *)errorAlert
{
    [self endBackgroundTask:_torrentDescriptors[torrent.infoHash]];

    torrent.error = translateError(errorAlert->error);
    if (!torrent.name) {
        // if the torrent metadata has not been downloaded yet
        torrent.name = [NSString stringWithUTF8String:errorAlert->torrent_name()];
    }
    [self send:[self torrentErrorNotification:torrent]];
}

- (void)onTorrentRemoved:(const lt::torrent_removed_alert *)removedAlert
{
    [self endBackgroundTask:_torrentDescriptors[[TorrentMetadata infoHash:removedAlert->info_hash]]];

    [self send: [self torrentRemovedNotification: removedAlert]];
    if (self.config.sessionAutoClose && ![self hasActiveTorrent]) {
        _quit = YES;
    }
}

- (BOOL)retriableError:(lt::error_code) error
{
    if (error.category() == lt::system_category()) {
        return (error == boost::system::errc::timed_out
             || error == boost::system::errc::connection_refused
             || error == boost::system::errc::address_not_available);

    } else if (error.category() == lt::http_category()) {
        return error.value() != 410;
    } else {
        return NO;
    }
}


#pragma mark iOS background task handling

- (BOOL) startBackgroundTask:(TorrentDescriptor *)descriptor
{
#if TARGET_OS_IPHONE
    if (_backgroundTasks[descriptor] != nil) {
        // already started
        return NO;
    }
    UIApplication *application = [UIApplication sharedApplication];
    NSString *taskName = [NSString stringWithFormat:@"Torrada-download-%@", descriptor.url.absoluteString];
    __block UIBackgroundTaskIdentifier identifier =
            [application beginBackgroundTaskWithName:taskName
                          expirationHandler:^{
        [self log:@"WARNING: background execution time expired for %@", descriptor];
        lt::torrent_handle handle = [self findHandle: descriptor];
        if (handle.is_valid()) {
            handle.save_resume_data();
        }
        [application endBackgroundTask:identifier];
    }];

    if (identifier != UIBackgroundTaskInvalid) {
        [self log:@"startBackgroundTask: %@ for %@", @(identifier), descriptor];
        _backgroundTasks[descriptor] = @(identifier);
        [self showNetworkActivityIndicator:YES];
        return YES;
    } else {
        [self log:@"could not start background task"];
    }
    return NO;
#else
    return NO;
#endif
}

- (BOOL) endBackgroundTask:(TorrentDescriptor *)descriptor
{
#if TARGET_OS_IPHONE
    if (!descriptor) {
        return NO;
    }
    NSNumber *value = _backgroundTasks[descriptor];
    if (value) {
        UIBackgroundTaskIdentifier identifier = (UIBackgroundTaskIdentifier)value.unsignedIntegerValue;
        [self log:@"endBackgroundTask: %@ for %@", @(identifier), descriptor];
        [[UIApplication sharedApplication] endBackgroundTask:identifier];
        [_backgroundTasks removeObjectForKey:descriptor];
        if (_backgroundTasks.count == 0) {
            [self showNetworkActivityIndicator: NO];
        }
        return YES;
    } else {
        [self log:@"endBackgroundTask: no task found for descriptor `%@`", descriptor];
    }
    return NO;
#else
    return NO;
#endif
}

#if TARGET_OS_IPHONE
- (void) showNetworkActivityIndicator:(BOOL) hasNetworkActivity {
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = hasNetworkActivity;
    });
}
#endif

#pragma mark plugin

static boost::shared_ptr<lt::torrent_plugin> create_plugin(lt::torrent_handle const& th, void *user_data)
{
    lt::torrent *t = th.native_handle().get();
    return boost::shared_ptr<lt::torrent_plugin>(new torrada_torrent_plugin(*t, user_data));
}

struct torrada_torrent_plugin final
        : lt::torrent_plugin
{
    torrada_torrent_plugin(lt::torrent& t, void *user_data)
        : m_torrent(t) {}

    virtual void on_state(int state) override
    {
        switch (state) {
            case lt::torrent_status::finished:
                m_torrent.stop_announcing();
                break;
            default: break;
        }
    }
    private:
    lt::torrent& m_torrent;
    torrada_torrent_plugin& operator=(torrada_torrent_plugin const&);
};

@end

#pragma mark helper methods

static NSError *translateError(lt::error_code error) {
    NSString *errorMessage = [NSString stringWithUTF8String:error.message().c_str()];
    NSString *errorCategory = [NSString stringWithUTF8String:error.category().name()];

    return [[NSError alloc] initWithDomain:errorCategory
                                      code:error.value()
                                  userInfo:@{NSLocalizedDescriptionKey: errorMessage}];
}

static NSString *torradaVersion()
{
    return [NSString stringWithFormat:@"Torrada %.1f (libtorrent/%s@%s, Boost/%s)",
        TorradaVersionNumber, lt::version(), LIBTORRENT_REVISION, BOOST_LIB_VERSION];
}

#import "Torrent.h"
#import "TorrentMetadata-Private.hpp"

#import <libtorrent/torrent_status.hpp>
#import <libtorrent/add_torrent_params.hpp>
#import <libtorrent/torrent.hpp>

namespace lt = libtorrent;

@implementation Torrent

- (NSString *)description
{
    return [NSString stringWithFormat:@"<Torrent: name=%@, state=%@, "
                                              "progress=%@, path=%@, infoHash=%@, url=%@>",
                                      self.name, self.stateString, self.progress, self.path, self.infoHash, self.url];
}

- (NSString *) stateString
{
    switch (self.state) {
        case TorrentStateCheckingFiles: return @"checking_files";;
        case TorrentStateDownloadingMetadata: return @"downloading_metadata";
        case TorrentStateDownloading: return @"downloading";
        case TorrentStateFinished: return @"finished";
        case TorrentStateSeeding: return @"seeding";
        case TorrentStateAllocating: return @"allocating";;
        case TorrentStateCheckingResumeData: return @"checking_resume_data";
        default: return @"unknown";
    }
}

+ (Torrent *)fromHandle:(lt::torrent_handle)handle
{
    return [self fromHandle:handle status:handle.status()];
}

+ (Torrent *)fromAddTorrentParams:(lt::add_torrent_params)addTorrentParams
{
    Torrent *torrent = [[Torrent alloc] init];
    torrent.infoHash = [TorrentMetadata infoHash: addTorrentParams.info_hash];
    if (!addTorrentParams.name.empty()) {
        torrent.name = [NSString stringWithUTF8String:addTorrentParams.name.c_str()];
    }
    if (!addTorrentParams.url.empty()) {
        torrent.url = [NSURL URLWithString:
                [NSString stringWithUTF8String:addTorrentParams.url.c_str()]];
    }
    return torrent;
}

+ (Torrent *)fromHandle:(lt::torrent_handle)handle
                 status:(lt::torrent_status)status
{
    Torrent *torrent = [[Torrent alloc] init];
    const lt::torrent_status::state_t torrent_state = status.state;
    torrent.name = [self name:handle];
    torrent.path = [self path:handle];
    torrent.state = (TorrentState) (torrent_state - 1  /*unused member*/);
    torrent.progress = [[NSProgress alloc] init];
    torrent.progress.totalUnitCount = 1000000;
    torrent.progress.completedUnitCount = status.progress_ppm;
    torrent.progress.pausable = YES;
    if (status.paused) {
        [torrent.progress pause];
    }

    torrent.progress.pausingHandler = ^{
        if (handle.is_valid()) {
            handle.pause();
        }
    };

    torrent.progress.resumingHandler = ^{
        if (handle.is_valid()) {
            handle.clear_error();
            handle.resume();
        }
    };
    [torrent.progress setUserInfoObject:@(status.download_rate) forKey:NSProgressThroughputKey];

    if (torrent_state != lt::torrent_status::checking_files &&
            torrent_state != lt::torrent_status::downloading_metadata) {
        // If this handle is to a torrent that hasn't loaded yet (for instance by being added)
        // by a URL, the returned value is undefined. (torrent_handle.hpp)
        //
        // If url is set, the info_hash is not actually the info-hash of the
        // torrent, but the hash of the URL, until we have the full torrent
        // only require the info-hash to match if we actually passed in one.
        // (session_impl.cpp:add_torrent_impl)
        torrent.infoHash = [TorrentMetadata infoHash:handle.info_hash()];
    }

    lt::torrent *native_torrent = handle.native_handle().get();
    if (native_torrent) {
        // TODO check if url is valid
        torrent.url =
                [NSURL URLWithString:[NSString stringWithUTF8String:native_torrent->url().c_str()]];
    }
    return torrent;
}

+ (NSString *)name:(lt::torrent_handle) handle
{
    boost::shared_ptr<lt::torrent_info const> torrent_info = handle.torrent_file();
    if (torrent_info) {
        return [NSString stringWithUTF8String:torrent_info->name().c_str()];
    } else {
        return nil;
    }
}

+ (NSString *)path:(lt::torrent_handle) handle
{
    return [[NSString stringWithCString:handle.status().save_path.c_str()
                               encoding:NSUTF8StringEncoding] stringByAppendingPathComponent:[self name:handle]];
}

@end


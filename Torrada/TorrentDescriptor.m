#import "TorrentDescriptor.h"

@implementation TorrentDescriptor
- (instancetype)initWithUrl:(NSURL *)url infoHash:(NSString *)infoHash urlSeed:(URLSeed *)urlSeed  {
    self = [super init];
    if (self) {
        _url = url;
        _infoHash = infoHash;
        _urlSeed = urlSeed;
    }
    return self;
}

- (NSString *)description {
    NSMutableString *description = [NSMutableString stringWithFormat:@"<%@: ", NSStringFromClass([self class])];
    [description appendFormat:@"self.url=%@", self.url];
    [description appendFormat:@", self.infoHash=%@", self.infoHash];
    [description appendFormat:@", self.urlSeed=%@", self.urlSeed];
    [description appendString:@">"];
    return description;
}

- (BOOL)isEqual:(id)other {
    if (other == self)
        return YES;
    if (!other || ![[other class] isEqual:[self class]])
        return NO;

    return [self isEqualToDescriptor:other];
}

- (BOOL)isEqualToDescriptor:(TorrentDescriptor *)descriptor {
    if (self == descriptor)
        return YES;
    if (descriptor == nil)
        return NO;
    if (self.url != descriptor.url && ![self.url isEqual:descriptor.url])
        return NO;
    if (self.infoHash != descriptor.infoHash && ![self.infoHash isEqualToString:descriptor.infoHash])
        return NO;
    return !(self.urlSeed != descriptor.urlSeed && ![self.urlSeed isEqual:descriptor.urlSeed]);
}

- (NSUInteger)hash {
    NSUInteger hash = [self.url hash];
    hash = hash * 31u + [self.infoHash hash];
    hash = hash * 31u + [self.urlSeed hash];
    return hash;
}

- (id)copyWithZone:(nullable NSZone *)zone {
    TorrentDescriptor *copy = (TorrentDescriptor *) [[[self class] allocWithZone:zone] init];
    if (copy) {
        copy->_url = _url;
        copy->_infoHash = _infoHash;
        copy->_urlSeed = _urlSeed;
    }
    return copy;
}

@end

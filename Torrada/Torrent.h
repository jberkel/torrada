#ifndef TORRENT_H_INCLUDED
#define TORRENT_H_INCLUDED

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, TorrentState) {
    // The torrent has not started its download yet, and is currently checking existing files.
    TorrentStateCheckingFiles,
    // The torrent is trying to download metadata from peers.
    TorrentStateDownloadingMetadata,
    // The torrent is being downloaded.
    TorrentStateDownloading,
    // In this state the torrent has finished downloading but still doesn't have the entire torrent.
    TorrentStateFinished,
    // In this state the torrent has finished downloading and is a pure seeder.
    TorrentStateSeeding,
    // If the torrent was started in full allocation mode, this indicates that the (disk) storage for
    // the torrent is allocated.
    TorrentStateAllocating,
    // The torrent is currently checking the fastresume data and
    // comparing it to the files on disk.
    TorrentStateCheckingResumeData
};


@interface Torrent : NSObject
// single file torrent: the proposed filename of the file
// multi file torrent: the proposed name of directory to store the files
// It is purely advisory. [http://www.bittorrent.org/beps/bep_0003.html]
@property (nullable, nonatomic) NSString *name;
@property (nullable, nonatomic) NSURL *url;
@property (nullable, nonatomic) NSError *error;
@property (nullable, nonatomic) NSString *infoHash;
// the full path to the saved file (or directory)
@property (nullable, nonatomic) NSString *path;
@property (nonatomic) TorrentState state;
@property (nonnull, nonatomic) NSProgress *progress;
@end

#endif

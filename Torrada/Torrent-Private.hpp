#ifndef TORRENT_PRIVATE_H_INCLUDED
#define TORRENT_PRIVATE_H_INCLUDED

#import <Foundation/Foundation.h>

#import "Torrent.h"
#import <libtorrent/torrent_handle.hpp>
#import <libtorrent/add_torrent_params.hpp>

@interface Torrent(Private)
+ (Torrent *)fromHandle:(libtorrent::torrent_handle)handle;
+ (Torrent *)fromHandle:(libtorrent::torrent_handle)handle status:(libtorrent::torrent_status)status;
+ (Torrent *)fromAddTorrentParams:(libtorrent::add_torrent_params)addTorrentParams;
+ (NSString *)name:(libtorrent::torrent_handle) handle;
@end

#endif

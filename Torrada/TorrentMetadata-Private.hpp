#ifndef TORRENT_METADATA_PRIVATE_H_INCLUDED
#define TORRENT_METADATA_PRIVATE_H_INCLUDED

#import "TorrentMetadata.h"

#import <libtorrent/sha1_hash.hpp>
#import <libtorrent/torrent_handle.hpp>

@interface TorrentMetadata(Private)
+ (TorrentMetadata * _Nonnull)metadataFor:(libtorrent::torrent_handle)handle;
/** converts the sha1 info_hash into a string */
+ (NSString * _Nonnull)infoHash:(libtorrent::sha1_hash)info_hash;
@end

#endif

#include "TorradaSettings.h"
#import <libtorrent/alert.hpp>
#import <boost/format.hpp>

using namespace libtorrent;

static int alertCategories(TorradaConfig config)
{
    int categories = alert::all_categories & ~(
        alert::dht_notification +
        alert::stats_notification
    );

    if (!config.debugEnabled) {
        categories = categories & ~(
            alert::session_log_notification +
            alert::torrent_log_notification +
            alert::peer_log_notification
        );
    }
    return categories;
}

libtorrent::settings_pack defaultSettings(TorradaConfig config, NSString *torradaVersion)
{
    settings_pack settings = settings_pack();
    settings.set_int(settings_pack::connections_limit, 10);
    settings.set_bool(settings_pack::close_redundant_connections, true);
    settings.set_int(settings_pack::active_loaded_limit, 20);
    settings.set_int(settings_pack::alert_queue_size, 5000);        // 1000
    settings.set_int(settings_pack::urlseed_wait_retry, 5);         // 30
    settings.set_int(settings_pack::urlseed_timeout, 10);           // 20
    settings.set_int(settings_pack::tracker_receive_timeout, 5);    // 10
    settings.set_int(settings_pack::tracker_completion_timeout, 10); // 60
    // delay = tracker_retry_delay_min (5) + tracker_retry_delay_min * backoff / 100 * fails^2
    settings.set_int(settings_pack::tracker_backoff, 25);           // 250
    settings.set_str(settings_pack::user_agent, torradaVersion.UTF8String);
    settings.set_bool(settings_pack::enable_lsd, false);
    settings.set_bool(settings_pack::enable_natpmp, false);
    settings.set_bool(settings_pack::enable_upnp, false);
    settings.set_str(settings_pack::listen_interfaces, (boost::format("0.0.0.0:%d") % config.listenPort).str());
    settings.set_int(settings_pack::alert_mask, alertCategories(config));

    NSURL *proxy = config.proxy ? [NSURL URLWithString:[NSString stringWithUTF8String:config.proxy]] : nil;
    if (proxy.host && proxy.port) {
        settings.set_str(settings_pack::proxy_hostname, std::string(proxy.host.UTF8String));
        settings.set_int(settings_pack::proxy_port, [proxy.port intValue]);
        if (proxy.user && proxy.password) {
            settings.set_str(settings_pack::proxy_username, std::string(proxy.user.UTF8String));
            settings.set_str(settings_pack::proxy_password, std::string(proxy.password.UTF8String));
            settings.set_int(settings_pack::proxy_type, settings_pack::proxy_type_t::http_pw);
        } else {
            settings.set_int(settings_pack::proxy_type, settings_pack::proxy_type_t::http);
        }
    }
    return settings;
}

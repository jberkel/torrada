#ifndef TORRADA_TORRADASETTINGS_H
#define TORRADA_TORRADASETTINGS_H

#include "Torrada.h"
#include "libtorrent/settings_pack.hpp"

libtorrent::settings_pack defaultSettings(TorradaConfig config, NSString *torradaVersion);

#endif

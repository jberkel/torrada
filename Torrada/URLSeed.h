#ifndef TORRENT_URLSEED_H_INCLUDED
#define TORRENT_URLSEED_H_INCLUDED

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, URLSeedMode) {
    // use unconditionally
    URLSeedModeUseAlways,
    // only use when connected on IPv6, for use with trackers which don't support it
    URLSeedModeOnlyOnIPv6,
    // only use when primary method fails
    URLSeedModeBackup
};

@interface URLSeed : NSObject
@property (nonnull, nonatomic, readonly) NSURL *url;
@property (readonly, nonatomic) URLSeedMode mode;

- (nonnull instancetype)initWithUrl:(nonnull NSURL *)url mode:(URLSeedMode)mode;
@end

#endif

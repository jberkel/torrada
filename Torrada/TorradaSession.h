#import <Foundation/Foundation.h>

#import "Torrent.h"
#import "TorrentDescriptor.h"

#define TORRADA_PROGRESS "TorradaProgress"
#define TORRADA_ADDED "TorradaAdded"
#define TORRADA_FINISHED "TorradaFinished"
#define TORRADA_REMOVED "TorradaRemoved"
#define TORRADA_ERROR "TorradaError"
#define TORRADA_SESSION_CLOSED "TorradaSessionClosed"

typedef struct {
    BOOL debugEnabled;
    BOOL removeTorrentsAfterDownload;
    /** automatically close the session when the last torrent has been downloaded */
    BOOL sessionAutoClose;
    /** the port to listen on. set to 0 to pick any available port */
    uint16_t listenPort;
    /** how often to retry failed connection, set to 0 for no limits */
    uint maxTrackerRetries;
    /** use a proxy to connect to peers and trackers */
    const char * _Nullable proxy;
} TorradaConfig;

typedef void(^TorradaLogger)(NSString * _Nonnull, va_list _Nonnull);

/** @return the currently active system proxy as URL (http://host:port) if one is set, or NULL */
FOUNDATION_EXPORT const char *_Nullable TorradaConfigSystemProxy(void) CF_SWIFT_NAME(TorradaConfig.systemProxy());

@interface TorradaSession : NSObject

@property (nonatomic, readonly) TorradaConfig config;
/** the path where the files are stored */
@property (nonnull, nonatomic, readonly) NSString *path;
/** optional logger */
@property (nullable, nonatomic, readwrite) TorradaLogger logger;

- (null_unspecified instancetype)init NS_UNAVAILABLE;
- (null_unspecified instancetype)initWithPath:(nonnull NSString *)path;
- (null_unspecified instancetype)initWithPath:(nonnull NSString *)path config:(TorradaConfig)config;

/**
 * Adds a torrent to the current session.
 * @return true if torrent was added to the session
 */
- (BOOL)addTorrent:(nonnull TorrentDescriptor *)torrentDescriptor;

/** @return the currently active torrents */
- (nonnull NSArray<Torrent *> *)torrents;

/** Removes a torrent from the session.
  @param torrentDescriptor the descriptor to remove
  @return the remove torrent or nil
 */
- (nullable Torrent *)removeTorrent:(nonnull TorrentDescriptor *)torrentDescriptor;

/** Starts the internal run loop. Needs to be called to start up the session. */
- (void)runLoop;

/**
 *  Closes the session. It will be invalid and the whole object should
 *  be discarded afterwards. This is an asynchronous operation (removing Torrents
 *  gracefully from the tracker can take a few seconds).
 *
 *  The TORRADA_SESSION_CLOSED notification will be sent once the session is closed.
 */
- (void)close;

- (BOOL)isClosed;


@end

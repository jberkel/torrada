#include "TorrentMetadata-Private.hpp"

#import <libtorrent/torrent_info.hpp>
#import <libtorrent/create_torrent.hpp>

namespace lt = libtorrent;

@implementation TorrentMetadata

+ (TorrentMetadata *)metadataFor:(lt::torrent_handle) handle
{
    NSAssert(handle.is_valid(), @"invalid torrent_handle");
    boost::shared_ptr<lt::torrent_info const> torrent_info = handle.torrent_file();
    lt::create_torrent ct(*torrent_info);
    lt::entry torrentEntry = ct.generate();
    std::vector<char> buffer;
    lt::bencode(std::back_inserter(buffer), torrentEntry);

    TorrentMetadata *metadata = [[TorrentMetadata alloc] init];
    metadata.name = [self name:handle];
    metadata.data = [NSData dataWithBytes:&buffer[0] length:buffer.size()];
    metadata.infoHash = [self infoHash:torrent_info->info_hash()];
    return metadata;
}

+ (TorrentMetadata *)create:(NSString *)name
                    tracker:(NSURL *)trackerURL
                      files:(NSArray<NSString *>*)files
{
    TorrentMetadata *metadata = [[TorrentMetadata alloc] init];
    std::string cname([name UTF8String]);

    lt::file_storage fs;
    for (NSString *file in files) {
        std::string path([file UTF8String]);
        fs.add_file(path, 1024);
    }

    lt::create_torrent ct(fs);
    ct.set_comment("created by Torrada");
    if (trackerURL) {
        std::string tracker([trackerURL.absoluteString UTF8String]);
        ct.add_tracker(tracker);
    }

    std::vector<char> buffer;
    lt::entry entry = ct.generate();
    lt::bencode(std::back_inserter(buffer), entry);

    boost::shared_ptr<lt::torrent_info> ti = boost::make_shared<lt::torrent_info>(&buffer[0], buffer.size());

    metadata.name = name;
    metadata.infoHash = [self infoHash:ti->info_hash()];
    metadata.data = [NSData dataWithBytes:&buffer[0] length:buffer.size()];

    return metadata;
}

- (NSString *)save:(NSString *)path
{
    NSAssert(self.data, @"no data set");
    NSString *target = [[path stringByAppendingPathComponent:self.name] stringByAppendingPathExtension:@"torrent"];
    [self.data writeToFile:target atomically:YES];

    return target;
}

+ (NSString *)infoHash:(lt::sha1_hash)info_hash
{
    return [NSString stringWithUTF8String:lt::to_hex(info_hash.to_string()).c_str()];
}

+ (NSString *)name:(lt::torrent_handle) handle
{
    boost::shared_ptr<lt::torrent_info const> torrent_info = handle.torrent_file();
    if (torrent_info) {
        return [NSString stringWithUTF8String:torrent_info->name().c_str()];
    } else {
        return nil;
    }
}

- (NSString *)description {
    NSMutableString *description = [NSMutableString stringWithFormat:@"<%@: ", NSStringFromClass([self class])];
    [description appendFormat:@"self.name=%@", self.name];
    [description appendFormat:@", self.infoHash=%@", self.infoHash];
    [description appendString:@">"];
    return description;
}

@end

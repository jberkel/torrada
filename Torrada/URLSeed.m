#import "URLSeed.h"

@implementation URLSeed
- (instancetype)initWithUrl:(NSURL *)url  mode:(URLSeedMode)mode {
    self = [super init];
    if (self) {
        _url = url;
        _mode = mode;
    }
    return self;
}
- (BOOL)isEqual:(id)other {
    if (other == self)
        return YES;
    if (!other || ![[other class] isEqual:[self class]])
        return NO;

    return [self isEqualToSeed:other];
}

- (BOOL)isEqualToSeed:(URLSeed *)seed {
    if (self == seed)
        return YES;
    if (seed == nil)
        return NO;
    if (self.url != seed.url && ![self.url isEqual:seed.url])
        return NO;
    return !(self.mode != seed.mode);
}

- (NSUInteger)hash {
    NSUInteger hash = [self.url hash];
    hash = hash * 31u + (NSUInteger) self.mode;
    return hash;
}

- (NSString *)description {
    NSMutableString *description = [NSMutableString stringWithFormat:@"<%@: ", NSStringFromClass([self class])];
    [description appendFormat:@"self.url=%@", self.url];
    [description appendFormat:@", self.mode=%@", self.modeString];
    [description appendString:@">"];
    return description;
}

- (NSString *)modeString
{
    switch (self.mode) {
        case URLSeedModeUseAlways: return @"useAlways";
        case URLSeedModeOnlyOnIPv6: return @"onlyOnIPv6";
        case URLSeedModeBackup: return @"backup";
        default: return nil;
    }
}

@end


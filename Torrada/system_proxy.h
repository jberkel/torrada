#import <Foundation/Foundation.h>
#ifdef __cplusplus
extern "C" {
#endif

NSURL * _Nullable systemProxy();

#ifdef __cplusplus
}
#endif

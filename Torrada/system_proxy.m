#import "system_proxy.h"

NSURL * _Nullable systemProxy()
{
    CFDictionaryRef proxySettings = CFNetworkCopySystemProxySettings();
    NSURL *proxyURL = nil;
    NSString *proxyHost = (NSString *)CFDictionaryGetValue(proxySettings, kCFNetworkProxiesHTTPProxy);
    NSNumber *proxyPort = (NSNumber *)CFDictionaryGetValue(proxySettings, kCFNetworkProxiesHTTPPort);
    NSNumber *proxyEnabled = (NSNumber *)CFDictionaryGetValue(proxySettings, kCFNetworkProxiesHTTPEnable);

    if ([proxyEnabled boolValue] &&
            [proxyHost length] > 0 &&
            [proxyPort intValue] >= 0 && [proxyPort intValue] < 65536) {
        NSURLComponents *components = [[NSURLComponents alloc] init];
        components.scheme = @"http";
        components.port = proxyPort;
        components.host = proxyHost;
        components.user = (NSString *)CFDictionaryGetValue(proxySettings, kCFProxyUsernameKey);
        components.password = (NSString *)CFDictionaryGetValue(proxySettings, kCFProxyPasswordKey);
        proxyURL = [components URL];
    }
    CFRelease(proxySettings);
    return proxyURL;
}

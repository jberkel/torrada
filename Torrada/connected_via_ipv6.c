#include <ifaddrs.h>
#include <arpa/inet.h>
#include <net/if.h>
#include "connected_via_ipv6.h"

#ifdef _IPV6_DEBUG
#include <stdio.h>
#endif

/**
 * Simple check to see if the device is connected only via IPv6:
 * Loop over all active, non-local, non-loopback network devices.
 * If there are no IPv4 addresses assigned but and at least one valid IPv6
 * address, assume the host is connected on a IPv6-only network.
 *
 * Returns 1 if likely connected via IPv6, else 0.
 */
int connected_via_ipv6()
{
    struct ifaddrs *interfaces;
    // On success, getifaddrs() returns zero; on error, -1 is returned, and errno is set appropriately.
    if (getifaddrs(&interfaces) != 0) {
        return 0;
    }
    int ipv4 = 0, ipv6 = 0;
    struct ifaddrs *interface;
    for (interface = interfaces; interface; interface = interface->ifa_next) {
        if (interface->ifa_addr == NULL
            || (interface->ifa_addr->sa_family != AF_INET && interface->ifa_addr->sa_family != AF_INET6)
            || !(interface->ifa_flags & IFF_UP)
            ||  (interface->ifa_flags & IFF_LOOPBACK)) {
            continue;
        }
        if (interface->ifa_addr->sa_family == AF_INET) {
            const struct sockaddr_in *addr4 = (const struct sockaddr_in *) interface->ifa_addr;
#ifdef _IPV6_DEBUG
            char addr4_p[INET_ADDRSTRLEN];
            inet_ntop(AF_INET, &addr4->sin_addr, addr4_p, sizeof addr4_p);
            fprintf(stderr, "IPv4: %s\n", addr4_p);
#endif
            if (!IN_LINKLOCAL(htonl(addr4->sin_addr.s_addr)) &&
                !IN_LOOPBACK(htonl(addr4->sin_addr.s_addr))) {
                ipv4++;
            }
        } else if (interface->ifa_addr->sa_family == AF_INET6) {
            const struct sockaddr_in6 *addr6 = (const struct sockaddr_in6 *) interface->ifa_addr;
#ifdef _IPV6_DEBUG
            char addr6_p[INET6_ADDRSTRLEN];
            inet_ntop(AF_INET6, &addr6->sin6_addr, addr6_p, sizeof addr6_p);
            fprintf(stderr, "IPv6: %s\n", addr6_p);
#endif
            if (!IN6_IS_ADDR_LINKLOCAL(&addr6->sin6_addr) &&
                !IN6_IS_ADDR_SITELOCAL(&addr6->sin6_addr) &&
                !IN6_IS_ADDR_LOOPBACK(&addr6->sin6_addr)) {
                ipv6++;
            }
        }
    }
    freeifaddrs(interfaces);
#ifdef _IPV6_DEBUG
    fprintf(stderr, "ipv4: %d, ipv6: %d\n", ipv4, ipv6);
#endif
    return ipv6 > 0 && ipv4 == 0;
}

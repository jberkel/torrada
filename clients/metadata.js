#!/usr/bin/env node

// fetch BitTorrent metadata via DHT + BEP9

const fetchMetadata = require('bep9-metadata-dl');

const infoHash = process.argv.length >= 3 ? process.argv[2] : 'a167f097de65d9435013d11943e8aba323c2d438';

fetchMetadata(infoHash, { maxConns: 10, fetchTimeout: 30000, socketTimeout: 5000 }, function(err, metadata) {
    if (err) {
        console.log(err);
    } else {
        console.log('[Callback] ' + metadata.info.name.toString('utf-8'));
    }
});

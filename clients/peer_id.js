#!/usr/bin/env node

// Decode and print peerid information

const Buffer = require('safe-buffer').Buffer;
const peerid = require('bittorrent-peerid');

if (process.argv.length >= 3) {
    const buffer = Buffer.from(process.argv[2], 'hex');
    console.log(buffer.toString('utf-8'));

    const parsed = peerid(process.argv[2]);
    console.log(parsed);
} else {
    console.log("peer_id.js <hex peerid>");
    process.exit(1);
}

#!/usr/bin/env node

// find peers for a BitTorrent infoHash via DHT

var DHT = require('bittorrent-dht');

const infoHash = process.argv.length >= 3 ? process.argv[2] : 'a167f097de65d9435013d11943e8aba323c2d438';
var dht = new DHT();

dht.listen(20000, function () {
    console.log('now listening')
});

dht.on('peer', function (peer, infoHash, from) {
    console.log('found potential peer ' + peer.host + ':' + peer.port + ' through ' + from.address + ':' + from.port)
});

// find peers for the given torrent info hash
dht.lookup(infoHash);

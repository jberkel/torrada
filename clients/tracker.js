#!/usr/bin/env node

const Client = require('bittorrent-tracker');
const crypto = require('crypto');

const infoHash = process.argv.length >= 3 ? process.argv[2] : 'a167f097de65d9435013d11943e8aba323c2d438';

const requiredOpts = {
    infoHash: Buffer.from(infoHash, 'hex'),
    peerId: Buffer.from(crypto.randomBytes(20)),
    announce: 'http://s3-tracker.eu-central-1.amazonaws.com:6969/announce',
    compact: false,
    port: 5000,
    userAgent: 'webtorrent-tracker',
    getAnnounceOpts: function () {
        return {
            left: 1
        }
    }
};

const client = new Client(requiredOpts);

client.on('error', function (err) {
    console.log(err.message)
});

client.on('warning', function (err) {
    console.log(err.message)
});

client.on('update', function (data) {
    console.log('got an announce response from tracker: ' + data.announce);
    console.log('number of seeders in the swarm: ' + data.complete);
    console.log('number of leechers in the swarm: ' + data.incomplete);
});

client.on('peer', function (addr) {
    console.log('found a peer: ' + addr)
});

// start getting peers from the tracker
client.start();
client.stop();

# Torrada

[![Build Status](https://www.bitrise.io/app/a4f7fb0da8549d81.svg?token=4xmnzDW4sQHFIl9B2Li9lQ&branch=master)](https://www.bitrise.io/app/a4f7fb0da8549d81)

An iOS/MacOS wrapper for [libtorrent][]. No documentation yet, but there is a sample client app and some tests.

Caveat emptor: Apple will very likely reject your app when shipping code which uses
BitTorrent as a transfer mechanism, even for legal content (“Your app falls into a category of
apps that is often used for illegal file sharing.”).

Therefore this code hasn't been used in production (yet).

## Installation

```bash
$ git clone https://gitlab.com/jberkel/torrada.git
$ git submodule init && git submodule update
$ make framework
```

[libtorrent]: http://www.libtorrent.org/

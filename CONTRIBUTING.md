# Contributing

Fork, then clone the repo:

    $ git clone git@gitlab.com:username/torrada.git

Make your modifications, be sure to add tests where it makes sense.

Verify that all tests pass on iOS and OSX:

    $ make test test-osx

Submit a merge request on gitlab.com.

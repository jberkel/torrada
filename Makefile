SHELL := /bin/bash
SCHEME := Torrada
BUILD_DIR := build-output
LIBTORRENT_DIR := Thirdparty/libtorrent
BOOST_DIR := Thirdparty/Boost
BOOST_FRAMEWORK := $(BOOST_DIR)/ios/framework/boost.framework
OPENSSL_FOR_IPHONE := Thirdparty/OpenSSL-for-iPhone
OPENSSL_FRAMEWORK := $(OPENSSL_FOR_IPHONE)/openssl.framework
XCODEBUILD := xcodebuild
XCODEBUILD_SETTINGS := ONLY_ACTIVE_ARCH=NO
FRAMEWORK := $(BUILD_DIR)/$(SCHEME).framework
GIT_REF := $(shell git show-ref -s --abbrev --head HEAD)
FRAMEWORK_ZIP := $(BUILD_DIR)/$(SCHEME)-$(GIT_REF).framework.zip
CONFIGURATION := Release
AWS := /usr/local/bin/aws
S3_BUCKET ?= s3://torrada/ios/
S3_REGION ?= eu-central-1

.PHONY: framework
framework: $(FRAMEWORK_ZIP)
.PHONY: boost
boost: $(BOOST_FRAMEWORK)

$(BOOST_FRAMEWORK):
	(pushd $(BOOST_DIR) && sh setup.sh && popd)

$(OPENSSL_FRAMEWORK):
	(pushd $(OPENSSL_FOR_IPHONE) && \
	./build-libssl.sh --archs="i386 x86_64 armv7 arm64" --verbose && \
	./create-openssl-framework.sh && popd)

$(FRAMEWORK): $(BOOST_FRAMEWORK) $(OPENSSL_FRAMEWORK)
	@pushd $(LIBTORRENT_DIR) && ./tools/set_version.py 1 1 3 0 && popd
	set -e ; \
	for sdk in iphoneos iphonesimulator; do \
		$(XCODEBUILD) $(XCODEBUILD_SETTINGS) build \
		-sdk $$sdk \
		-scheme $(SCHEME) \
		-configuration $(CONFIGURATION) \
		-derivedDataPath $(BUILD_DIR) \
		BITCODE_GENERATION_MODE=bitcode \
		PLATFORM_NAME=$$sdk ; \
	done
	@rm -rf $(FRAMEWORK)
	@cp -r $(BUILD_DIR)/Build/Products/$(CONFIGURATION)-iphoneos/$(SCHEME).framework $(BUILD_DIR)
	xcrun lipo -create \
		$(BUILD_DIR)/Build/Products/$(CONFIGURATION)-iphonesimulator/$(SCHEME).framework/$(SCHEME) \
		$(BUILD_DIR)/Build/Products/$(CONFIGURATION)-iphoneos/$(SCHEME).framework/$(SCHEME) \
		-output $(FRAMEWORK)/$(SCHEME)

$(FRAMEWORK_ZIP): $(FRAMEWORK)
	@rm -f $@
	cd $(BUILD_DIR) && \
	zip -qr $(notdir $@) $(notdir $<) && \
	for file in Build/Products/$(CONFIGURATION)-iphoneos/*.bcsymbolmap; do \
		zip --junk-paths $(notdir $@) $$file; \
	done

test: $(BOOST_FRAMEWORK) $(OPENSSL_FRAMEWORK)
	$(XCODEBUILD) test \
		-sdk iphonesimulator \
		-scheme $(SCHEME) \
		-derivedDataPath $(BUILD_DIR) \
		-destination 'platform=iOS Simulator,name=iPhone 4s'

test-osx: $(BOOST_FRAMEWORK)
	$(XCODEBUILD) test \
		-sdk macosx \
		-scheme '$(SCHEME) Mac' \
		-derivedDataPath $(BUILD_DIR)

.PHONY: upload
upload: $(FRAMEWORK_ZIP) $(AWS)
	$(AWS) s3 cp --acl public-read --region $(S3_REGION) $< $(S3_BUCKET)

$(AWS):
	brew install awscli

.PHONY: sampleClient
sampleClient: $(BOOST_FRAMEWORK) $(OPENSSL_FRAMEWORK)
	$(XCODEBUILD) build -sdk iphonesimulator -scheme sampleClient -derivedDataPath $(BUILD_DIR)

clean:
	rm -rf $(BUILD_DIR)

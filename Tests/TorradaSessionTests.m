#import <XCTest/XCTest.h>
#import <Torrada/TorradaSession.h>
#import <netdb.h>

static const int DEFAULT_TEST_TIMEOUT = 45;
static NSString *TEST_FILE = @"http://s3.dualstack.us-west-2.amazonaws.com/torrentlib-test/test.txt";
static NSString *TEST_FILE_HTTPS = @"https://s3.dualstack.us-west-2.amazonaws.com/torrentlib-test/test.txt";
static NSString *TEST_NAME= @"test.txt";
static NSString *TEST_HASH = @"0786821eb7e8435d0855627d88dac455ed36af49";
static NSString *VALID_HASH = @"cdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcd";


@interface TorradaSessionTests : XCTestCase
@property (nonatomic) NSURL *torrentURL;
@property (nonatomic) NSURL *torrentHTTPSURL;
@property (nonatomic) TorradaSession *session;
@property (nonatomic) TorradaConfig config;
@end

@implementation TorradaSessionTests

static NSString *tempPath()
{
    NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:[[NSUUID UUID] UUIDString]];
    [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    return path;
}

- (void)setUp
{
    [super setUp];
    self.config = (TorradaConfig) {
        .debugEnabled = NO,
        .removeTorrentsAfterDownload = YES,
        .sessionAutoClose = NO
    };
    self.torrentURL = [NSURL URLWithString:[TEST_FILE stringByAppendingString:@"?torrent"]];
    self.torrentHTTPSURL = [NSURL URLWithString:[TEST_FILE_HTTPS stringByAppendingString:@"?torrent"]];
}

- (void)tearDown {
    [super tearDown];
    if (self.session && !self.session.isClosed) {
        XCTestExpectation *expectation = [self expectationWithDescription:@"sessionClose"];
        [self waitForNotification:@TORRADA_SESSION_CLOSED
                      expectation:expectation
                           action:^{ [self.session close]; }
            notificationCondition:nil];
    }
    self.session = nil;
}

#pragma mark tests

- (void)testDownloadTorrent
{
    [self downloadTorrent:self.torrentURL infoHash:TEST_HASH urlSeed:nil];
}

- (void)testDownloadTorrentWithUrlSeed
{
    URLSeed *urlSeed = [[URLSeed alloc] initWithUrl:[NSURL URLWithString:TEST_FILE] mode:URLSeedModeUseAlways];
    [self downloadTorrent:self.torrentURL infoHash:TEST_HASH urlSeed:urlSeed];
}

- (void)testDownloadTorrentWithUrlSeedBackup
{
    URLSeed *urlSeed = [[URLSeed alloc] initWithUrl:[NSURL URLWithString:TEST_FILE] mode:URLSeedModeBackup];
    [self downloadTorrent:self.torrentURL infoHash:TEST_HASH urlSeed:urlSeed];
}

- (void)testDownloadTorrentWithUrlSeedErrorAndRetry
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"runLoopWait"];
    self.session = [[TorradaSession alloc] initWithPath:tempPath() config:self.config];
    NSURL *invalidURL = [NSURL URLWithString:@"http://invalid.example.com/foo"];
    URLSeed *urlSeed = [[URLSeed alloc] initWithUrl:invalidURL mode:URLSeedModeUseAlways];
    TorrentDescriptor *descriptor = [[TorrentDescriptor alloc] initWithUrl:invalidURL infoHash:VALID_HASH urlSeed:urlSeed];

    BOOL added = [self.session addTorrent:descriptor];
    XCTAssert(added);
    NSNotification *error = [self waitForNotification:@TORRADA_ERROR
                                          expectation:expectation
                                               action:^{[self.session runLoop];}
                                notificationCondition:nil];

    Torrent *torrent = error.userInfo[@"torrent"];
    XCTAssertEqualObjects(invalidURL.absoluteString, torrent.name);
    XCTAssertEqualObjects(@"Host not found (authoritative)", torrent.error.localizedDescription);

    expectation = [self expectationWithDescription:@"runLoopWait"];
    // retry
    [torrent.progress resume];

    error = [self waitForNotification:@TORRADA_ERROR
                          expectation:expectation
                               action:^{ [self.session runLoop]; }
                notificationCondition:nil];

    torrent = error.userInfo[@"torrent"];
    XCTAssertEqualObjects(@"Host not found (authoritative)", torrent.error.localizedDescription);
}

- (void)testDownloadTorrentFromHTTPSURL
{
#if TARGET_OS_IPHONE
    [self downloadTorrent:self.torrentHTTPSURL infoHash:TEST_HASH urlSeed:nil];
#else
    NSLog(@"ignored on OSX");
#endif
}

- (void)testDownloadTorrentWithFileURLNotFound
{
    NSURL *fileURL = [NSURL URLWithString:@"file:///foo"];
    TorrentDescriptor *descriptor = [[TorrentDescriptor alloc] initWithUrl:fileURL infoHash:VALID_HASH urlSeed:nil];
    Torrent *torrent = [self startDownload:descriptor andWaitForNotification:@TORRADA_ERROR];
    XCTAssertNotNil(torrent.error);
    XCTAssertEqualObjects(@"No such file or directory", torrent.error.localizedDescription);
}

- (void)testAddTorrentTwiceToSession
{
    self.session = [[TorradaSession alloc] initWithPath:tempPath() config:self.config];
    TorrentDescriptor *descriptor = [[TorrentDescriptor alloc] initWithUrl:self.torrentURL infoHash:TEST_HASH urlSeed:nil];
    XCTAssert([self.session addTorrent:descriptor]);
    XCTAssertFalse([self.session addTorrent:descriptor]);
}

- (void)testErrorHandlingNotFound
{
    self.session = [[TorradaSession alloc] initWithPath:tempPath() config:self.config];
    NSURL *url = [[NSURL alloc] initWithString:@"http://notfound.example.com/"];
    TorrentDescriptor *descriptor = [[TorrentDescriptor alloc] initWithUrl:url infoHash:VALID_HASH urlSeed:nil];

    XCTestExpectation *expectation = [self expectationWithDescription:@"runLoopWait"];
    BOOL added = [self.session addTorrent:descriptor];
    XCTAssert(added);
    NSNotification *error = [self waitForNotification:@TORRADA_ERROR
                                          expectation:expectation
                                               action:^{[self.session runLoop];}
                                notificationCondition:nil];

    Torrent *torrent = error.userInfo[@"torrent"];

    XCTAssertNotNil(torrent.error);
    XCTAssertEqualObjects(@"Host not found (authoritative)", torrent.error.localizedDescription);
    XCTAssertEqualObjects(@"asio.netdb", torrent.error.domain);
    XCTAssertEqual(HOST_NOT_FOUND, torrent.error.code);
}

- (void)testErrorHandlingUnsupportedProtocol
{
    self.session = [[TorradaSession alloc] initWithPath:tempPath() config:self.config];
    NSURL *url = [[NSURL alloc] initWithString:@"httpz://unsupported.example.com/"];
    TorrentDescriptor *descriptor = [[TorrentDescriptor alloc] initWithUrl:url infoHash:VALID_HASH urlSeed:nil];

    XCTestExpectation *expectation = [self expectationWithDescription:@"runLoopWait"];
    BOOL added = [self.session addTorrent:descriptor];
    XCTAssert(added);
    NSNotification *error = [self waitForNotification:@TORRADA_ERROR
                                          expectation:expectation
                                               action:^{[self.session runLoop];}
                                notificationCondition:nil];

    Torrent *torrent = error.userInfo[@"torrent"];

    XCTAssertNotNil(torrent.error);
    XCTAssertEqualObjects(@"unsupported URL protocol", torrent.error.localizedDescription);
    XCTAssertEqualObjects(@"libtorrent", torrent.error.domain);
    XCTAssertEqual(24 /* libtorrent::errors::error_code_enum.unsupported_url_protocol */, torrent.error.code);
}


- (void)testRemoveTorrentFromSession
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"finishedWait"];

    TorradaConfig config = { .debugEnabled =  NO, .removeTorrentsAfterDownload = NO };
    self.session = [[TorradaSession alloc] initWithPath:tempPath() config:config];
    TorrentDescriptor *descriptor = [[TorrentDescriptor alloc] initWithUrl:self.torrentURL infoHash:TEST_HASH urlSeed:nil];
    XCTAssert([self.session addTorrent:descriptor]);
    NSNotification *finished = [self waitForNotification:@TORRADA_FINISHED
                                             expectation:expectation
                                                  action:^{ [self.session runLoop];}
                                   notificationCondition:nil];
    XCTAssertNotNil(finished.userInfo[@"torrent"]);

    expectation = [self expectationWithDescription:@"removeWait"];

    NSNotification *removed =[self waitForNotification:@TORRADA_REMOVED
                  expectation:expectation
                       action:^{
                           Torrent *torrent = [self.session removeTorrent:descriptor];
                           XCTAssertNotNil(torrent);
                           XCTAssertEqualObjects(TEST_NAME, torrent.name);
                           XCTAssertEqualObjects(TEST_HASH, torrent.infoHash);
                       }
       notificationCondition:nil];

    NSString *infoHash = removed.userInfo[@"info_hash"];
    NSURL *url = removed.userInfo[@"url"];
    XCTAssertEqualObjects(TEST_HASH, infoHash);
    XCTAssertEqualObjects(self.torrentURL, url);

}

- (void)testTorradaAddedNotification
{
    TorradaConfig config = {.debugEnabled =  NO, .removeTorrentsAfterDownload = YES};
    self.session = [[TorradaSession alloc] initWithPath:tempPath() config:config];
    TorrentDescriptor *descriptor = [[TorrentDescriptor alloc] initWithUrl:self.torrentURL infoHash:TEST_HASH urlSeed:nil];
    XCTAssert([self.session addTorrent:descriptor]);
    XCTestExpectation *expectation = [self expectationWithDescription:@"removeWait"];

    NSNotification *added = [self waitForNotification:@TORRADA_ADDED
                  expectation:expectation
                       action:^{ [self.session runLoop]; }
        notificationCondition:nil];

    Torrent *torrent = added.userInfo[@"torrent"];
    XCTAssertEqualObjects(self.torrentURL, torrent.url);
}

- (void)testTorrentsAreAutomaticallyRemovedAfterDownloading
{
    TorradaConfig config = {.debugEnabled =  NO, .removeTorrentsAfterDownload = YES};
    self.session = [[TorradaSession alloc] initWithPath:tempPath() config:config];
    TorrentDescriptor *descriptor = [[TorrentDescriptor alloc] initWithUrl:self.torrentURL infoHash:TEST_HASH urlSeed:nil];
    XCTAssert([self.session addTorrent:descriptor]);
    XCTestExpectation *expectation = [self expectationWithDescription:@"removeWait"];

    [self waitForNotification:@TORRADA_REMOVED
                  expectation:expectation
                       action:^{ [self.session runLoop]; }
        notificationCondition:nil];
}

- (void)testFastResumeDataGetsSavedAfterFinishing
{
    TorradaConfig config = {.debugEnabled =  NO, .removeTorrentsAfterDownload = YES};
    NSString *path = tempPath();
    self.session = [[TorradaSession alloc] initWithPath:path config:config];
    TorrentDescriptor *descriptor = [[TorrentDescriptor alloc] initWithUrl:self.torrentURL infoHash:TEST_HASH urlSeed:nil];
    XCTAssert([self.session addTorrent:descriptor]);
    XCTestExpectation *expectation = [self expectationWithDescription:@"removeWait"];

    [self waitForNotification:@TORRADA_REMOVED
                  expectation:expectation
                       action:^{ [self.session runLoop]; }
            notificationCondition:nil];

    NSArray *contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];

    NSString *resumeFile = [TEST_NAME stringByAppendingString:@".resume"];
    XCTAssertTrue([contents containsObject:resumeFile]);

    NSString *resume = [path stringByAppendingPathComponent:resumeFile];
    XCTAssert([[NSFileManager defaultManager] fileExistsAtPath:resume]);
    unsigned long long size = [[[NSFileManager defaultManager] attributesOfItemAtPath:resume error:nil] fileSize];
    XCTAssertGreaterThan(size, 0);
}

- (void)testDownloadNonexistentTorrentFileProducesError
{
    self.session = [[TorradaSession alloc] initWithPath:tempPath() config:self.config];
    XCTestExpectation *expectation = [self expectationWithDescription:@"waitError"];
    NSURL *url = [NSURL URLWithString:@"http://s3-us-west-2.amazonaws.com/torrentlib-test/does.not.exist?torrent"];
    BOOL added = [self.session addTorrent:[[TorrentDescriptor alloc] initWithUrl:url infoHash:VALID_HASH urlSeed:nil]];
    XCTAssert(added);

    NSNotification *error = [self waitForNotification:@TORRADA_ERROR
                                          expectation:expectation
                                               action:^{ [self.session runLoop]; }
                                notificationCondition:nil];

    Torrent *torrent = error.userInfo[@"torrent"];
    XCTAssertEqualObjects(@"403 Forbidden", torrent.error.localizedDescription);
    XCTAssertEqualObjects(@"http://s3-us-west-2.amazonaws.com/torrentlib-test/does.not.exist?torrent", torrent.url.absoluteString);
    XCTAssertEqualObjects(@"http://s3-us-west-2.amazonaws.com/torrentlib-test/does.not.exist?torrent", torrent.name);
}

- (void)testDownloadWithProxy
{
    self.config = (TorradaConfig) {.proxy = "http://127.0.0.1:33333"};
    self.session = [[TorradaSession alloc] initWithPath:tempPath() config:self.config];

    TorrentDescriptor *descriptor = [[TorrentDescriptor alloc] initWithUrl:self.torrentURL infoHash:TEST_HASH urlSeed:nil];
    XCTAssert([self.session addTorrent:descriptor]);
    XCTestExpectation *expectation = [self expectationWithDescription:@"waitError"];
    NSNotification *error = [self waitForNotification:@TORRADA_ERROR
                                          expectation:expectation
                                               action:^{ [self.session runLoop]; }
                                notificationCondition:nil];
    Torrent *torrent = error.userInfo[@"torrent"];
    XCTAssertEqualObjects(@"Connection refused", torrent.error.localizedDescription);
}

- (void)testDownloadWithPasswordProtectedProxy
{
    self.config = (TorradaConfig) {.proxy = "http://127.0.0.1:33333"};
    self.session = [[TorradaSession alloc] initWithPath:tempPath() config:self.config];

    TorrentDescriptor *descriptor = [[TorrentDescriptor alloc] initWithUrl:self.torrentURL infoHash:TEST_HASH urlSeed:nil];
    XCTAssert([self.session addTorrent:descriptor]);
    XCTestExpectation *expectation = [self expectationWithDescription:@"waitError"];
    NSNotification *error = [self waitForNotification:@TORRADA_ERROR
                                          expectation:expectation
                                               action:^{ [self.session runLoop]; }
                                notificationCondition:nil];
    Torrent *torrent = error.userInfo[@"torrent"];
    XCTAssertEqualObjects(@"Connection refused", torrent.error.localizedDescription);
}

- (void)IGNORED_testDownloadHttpsURLWithProxy
{
#if TARGET_OS_IPHONE
    self.config = (TorradaConfig) {.proxy = "http://127.0.0.1:33333"};
    self.session = [[TorradaSession alloc] initWithPath:tempPath() config:self.config];
    URLSeed *urlSeed = nil;// [[URLSeed alloc] initWithUrl:[NSURL URLWithString:TEST_FILE_HTTPS] mode:URLSeedModeBackup];

    [self downloadTorrent:self.torrentURL infoHash:TEST_HASH urlSeed:urlSeed];
#endif
}

#pragma mark helper

- (Torrent *)startDownload:(TorrentDescriptor *)descriptor andWaitForNotification:(NSString *)notification
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"runLoopWait"];

    self.session = [[TorradaSession alloc] initWithPath:tempPath() config:self.config];
    BOOL added = [self.session addTorrent:descriptor];
    XCTAssert(added);
    NSNotification *receivedNotification = [self waitForNotification:notification
                                                         expectation:expectation
                                                              action:^{ [self.session runLoop]; }
                                               notificationCondition:nil];
    Torrent *torrent = receivedNotification.userInfo[@"torrent"];
    return torrent;
}

- (Torrent *)downloadTorrent:(NSURL *)url infoHash:(NSString * _Nonnull) infoHash urlSeed:(URLSeed *)urlSeed
{
    TorrentDescriptor *descriptor = [[TorrentDescriptor alloc] initWithUrl:url infoHash:infoHash urlSeed:urlSeed];
    Torrent *torrent = [self startDownload:descriptor andWaitForNotification:@TORRADA_FINISHED];

    NSArray *torrents = [self.session torrents];
    XCTAssertEqual(1, [torrents count]);
    XCTAssertEqualObjects(TEST_NAME, [ (Torrent*) torrents[0] name]);
    XCTAssertEqualObjects(TEST_NAME, torrent.name);
    XCTAssertNil(torrent.error);
    XCTAssertEqualObjects(TEST_HASH, torrent.infoHash);
    XCTAssertEqual(TorrentStateSeeding, torrent.state);
    XCTAssertEqualObjects(TEST_NAME, [torrent.path lastPathComponent]);
    XCTAssertEqualObjects(url, torrent.url);
    XCTAssert([[NSFileManager defaultManager] fileExistsAtPath:torrent.path]);
    return torrent;
}

- (NSNotification *)waitForNotification:(NSString *)name
                            expectation:(XCTestExpectation *)expectation
                                 action:(void (^)())action
                  notificationCondition:(BOOL (^)(NSNotification *))condition
{
    __block NSNotification *notification;
    id token = [[NSNotificationCenter defaultCenter] addObserverForName:name object:nil queue:nil usingBlock:^(NSNotification *note) {
        notification = note;
        if (condition == nil || condition(notification)) {
            [expectation fulfill];
        }
    }];

    dispatch_async(dispatch_get_main_queue(), action);

    [self waitForExpectationsWithTimeout:DEFAULT_TEST_TIMEOUT handler:^(NSError *error) {
        XCTAssertNil(error, @"timeout (> %d secs): %@", DEFAULT_TEST_TIMEOUT, error);
    }];

    [[NSNotificationCenter defaultCenter] removeObserver:token];

    return notification;
}

@end

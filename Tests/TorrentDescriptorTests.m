#import <XCTest/XCTest.h>
#import <Torrada/TorradaSession.h>

@interface TorrentDescriptorTests : XCTestCase
@end

@implementation TorrentDescriptorTests

- (void)testEqualsAndHash
{
    TorrentDescriptor *td1 = [[TorrentDescriptor alloc] initWithUrl:[NSURL URLWithString:@"foo"] infoHash:@"12345" urlSeed:nil];
    TorrentDescriptor *td2 = [[TorrentDescriptor alloc] initWithUrl:[NSURL URLWithString:@"foo"] infoHash:@"12345" urlSeed:nil];
    XCTAssertEqualObjects(td1, td2);
    XCTAssertEqual(td1.hash, td2.hash);
}

- (void)testEqualsAndHashEmpty
{
    TorrentDescriptor *td1 = [[TorrentDescriptor alloc] initWithUrl:nil infoHash:@"" urlSeed:nil];
    TorrentDescriptor *td2 = [[TorrentDescriptor alloc] initWithUrl:nil infoHash:@"" urlSeed:nil];
    XCTAssertEqualObjects(td1, td2);
    XCTAssertEqual(td1.hash, td2.hash);
}

- (void)testEqualsAndHashNonMatchUrl
{
    TorrentDescriptor *td1 = [[TorrentDescriptor alloc] initWithUrl:[NSURL URLWithString:@"baz"] infoHash:@"12345" urlSeed:nil];
    TorrentDescriptor *td2 = [[TorrentDescriptor alloc] initWithUrl:[NSURL URLWithString:@"foo"] infoHash:@"12345" urlSeed:nil];
    XCTAssertNotEqualObjects(td1, td2);
    XCTAssertNotEqual(td1.hash, td2.hash);
}

- (void)testEqualsAndHashNonMatchInfoHash
{
    TorrentDescriptor *td1 = [[TorrentDescriptor alloc] initWithUrl:[NSURL URLWithString:@"foo"] infoHash:@"12345" urlSeed:nil];
    TorrentDescriptor *td2 = [[TorrentDescriptor alloc] initWithUrl:[NSURL URLWithString:@"foo"] infoHash:@"67890" urlSeed:nil];
    XCTAssertNotEqualObjects(td1, td2);
    XCTAssertNotEqual(td1.hash, td2.hash);
}

- (void)testEqualsAndHashNonMatchUrlSeed
{
    TorrentDescriptor *td1 = [[TorrentDescriptor alloc] initWithUrl:[NSURL URLWithString:@"foo"] infoHash:@"12345" urlSeed:[[URLSeed alloc] initWithUrl:[NSURL URLWithString:@"foo"] mode:URLSeedModeUseAlways]];
    TorrentDescriptor *td2 = [[TorrentDescriptor alloc] initWithUrl:[NSURL URLWithString:@"foo"] infoHash:@"12345" urlSeed:nil];
    XCTAssertNotEqualObjects(td1, td2);
    XCTAssertNotEqual(td1.hash, td2.hash);
}

@end

#import <XCTest/XCTest.h>
#import <Torrada/TorradaSession.h>
#import "TorrentMetadata.h"
#import <GCDWebServers/GCDWebServer.h>

static NSString *VALID_HASH = @"cdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcdcd";

@interface TorradaIntegrationTests : XCTestCase
@property (nonatomic) TorradaSession *session;
@property (nonatomic) TorradaConfig config;
@property (nonatomic) NSUInteger port;
@property (nonatomic) NSURL *trackerURL;
@property (nonatomic) NSString *path;
@property (nonnull, nonatomic) GCDWebServer *webServer;
@end

@implementation TorradaIntegrationTests

static const int DEFAULT_TEST_TIMEOUT = 120;

- (void)setUp
{
    [super setUp];
    self.config = (TorradaConfig) {
        .debugEnabled = NO,
        .removeTorrentsAfterDownload = YES,
        .sessionAutoClose = NO,
        .maxTrackerRetries = 3
    };
    self.port = 8080;
    self.path = [self tempPath];
    self.webServer = [[GCDWebServer alloc] init];
    self.trackerURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://localhost:%li/announce", (long) self.port]];
    self.session = [[TorradaSession alloc] initWithPath:self.path config:self.config];
    XCTAssert([self.webServer startWithPort:self.port bonjourName:nil], @"error starting web server");
}

- (void)tearDown
{
    [super tearDown];
    [self.webServer stop];
    if (self.session && !self.session.isClosed) {
        XCTestExpectation *expectation = [self expectationWithDescription:@"sessionClose"];
        [self waitForNotification:@TORRADA_SESSION_CLOSED
                      expectation:expectation
                           action:^{ [self.session close]; }
                          timeout:5.0];
    }
    self.session = nil;
}

- (void)IGNORE_testMetadataDownloadHangs
{
    [self webServerNeverReturns];
    TorrentDescriptor *descriptor = [[TorrentDescriptor alloc] initWithUrl:self.trackerURL infoHash:VALID_HASH urlSeed:nil];
    Torrent *torrent = [self startDownload:descriptor andWaitForNotification:@TORRADA_ERROR];
    XCTAssertEqualObjects(@"Operation timed out", torrent.error.localizedDescription);
}

- (void)testTrackerHangs
{
    [self webServerNeverReturns];
    Torrent *torrent = [self startDownloadAndWaitForNotification:@TORRADA_ERROR];
    XCTAssertEqualObjects(@"Operation timed out", torrent.error.localizedDescription);
}

- (void)testTrackerRespondsError500
{
    [self webServerReturnsStatus:500];
    Torrent *torrent = [self startDownloadAndWaitForNotification:@TORRADA_ERROR];
    XCTAssertEqualObjects(@"500 Internal Server Error", torrent.error.localizedDescription);
}

#pragma mark helper

- (void)webServerNeverReturns
{
    [self.webServer addDefaultHandlerForMethod:@"GET"
                                  requestClass:GCDWebServerRequest.class
                                  processBlock:^GCDWebServerResponse *(GCDWebServerRequest *request) {
                                      sleep(INT_MAX);
                                      return nil;
                                  }];
}

- (void)webServerReturnsStatus:(int)statusCode
{
    [self.webServer addDefaultHandlerForMethod:@"GET"
                                  requestClass:GCDWebServerRequest.class
                                  processBlock:^GCDWebServerResponse *(GCDWebServerRequest *request) {
                                      return [GCDWebServerResponse responseWithStatusCode:statusCode];
                                  }];

}

- (Torrent *)startDownloadAndWaitForNotification:(NSString *)notification
{
    TorrentMetadata *md = [TorrentMetadata create:@"test" tracker:self.trackerURL files:@[@"test"]];
    NSString *torrentFile = [md save:self.path];
    TorrentDescriptor *td = [[TorrentDescriptor alloc] initWithUrl:[NSURL fileURLWithPath:torrentFile] infoHash:md.infoHash urlSeed:nil];
    return [self startDownload:td andWaitForNotification:notification];
}

- (Torrent *)startDownload:(TorrentDescriptor *)descriptor andWaitForNotification:(NSString *)notification
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"runLoopWait"];
    XCTAssert([self.session addTorrent:descriptor]);

    NSNotification *receivedNotification = [self waitForNotification:notification
                                                         expectation:expectation
                                                              action:^{ [self.session runLoop];}
                                                             timeout:DEFAULT_TEST_TIMEOUT];
    Torrent *torrent = receivedNotification.userInfo[@"torrent"];
    return torrent;
}


- (NSString *)tempPath
{
    NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:[[NSUUID UUID] UUIDString]];
    [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    return path;
}

- (NSNotification *)waitForNotification:(NSString *)name
                            expectation:(XCTestExpectation *)expectation
                                 action:(void (^)())action
                                timeout:(NSTimeInterval)timeout
{
    __block NSNotification *notification;
    id token = [[NSNotificationCenter defaultCenter] addObserverForName:name object:nil queue:nil usingBlock:^(NSNotification *note) {
        notification = note;
        [expectation fulfill];
    }];

    dispatch_async(dispatch_get_main_queue(), action);

    [self waitForExpectationsWithTimeout:timeout handler:^(NSError *error) {
        XCTAssertNil(error, @"timeout (> %f secs): %@", timeout, error);
    }];

    [[NSNotificationCenter defaultCenter] removeObserver:token];

    return notification;
}

@end

#import <XCTest/XCTest.h>

#import "TorrentMetadata.h"

@interface TorradaMetadataTests : XCTestCase
@end

@implementation TorradaMetadataTests

static NSString *tempPath()
{
    NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:[[NSUUID UUID] UUIDString]];
    [NSFileManager.defaultManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    return path;
}

- (void)testCreateMetadata
{
    TorrentMetadata *metadata = [TorrentMetadata create:@"test"
                                                tracker:[NSURL URLWithString:@"http://example.com:6969"]
                                                  files:@[@"foo"]];
    XCTAssertEqualObjects(@"test", metadata.name);

    NSString *path = tempPath();
    NSString *torrentFile = [metadata save:path];
    NSLog(@"saved to %@", torrentFile);

    XCTAssert([NSFileManager.defaultManager fileExistsAtPath:torrentFile]);
}

@end

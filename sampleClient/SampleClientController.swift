import UIKit
import Torrada

class SampleClientController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableView:UITableView?

    struct Download {
        let descriptor: TorrentDescriptor
        let state: String
        let progress: Progress
        let rate: Int
        let error: String?
    }

    var downloads = (NSArray(contentsOf:
        Bundle.main.url(forResource: "DownloadList", withExtension: "plist")!)! as! [[String:String]]).map { dict in
        Download(
            descriptor: TorrentDescriptor(
                    url: URL(string: dict["torrent-url"]!)!,
                    infoHash: dict["info-hash"]!,
                    urlSeed: URLSeed(url: URL(string: dict["url-seed"]!)!, mode: .onlyOnIPv6)),
            state: "",
            progress: Progress(totalUnitCount: 0),
            rate: 0,
            error: nil)
    }

    var torrada: TorradaSession?

    override func viewDidLoad() {
        super.viewDidLoad()

        let path = NSTemporaryDirectory().stringByAppendingPathComponent("torradaSampleClient")
        if !FileManager.default.fileExists(atPath: path) {
            try! FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
        }
        try! clearData(path)

        tableView?.dataSource = self
        tableView?.rowHeight = 55

        torrada = TorradaSession(
            path: path,
            config: TorradaConfig(
                debugEnabled: false,
                removeTorrentsAfterDownload: true,
                sessionAutoClose: true,
                listenPort: 0,
                maxTrackerRetries: 0,
                proxy: TorradaConfig.systemProxy()
            )
        )

        for download in downloads {
            torrada?.addTorrent(download.descriptor)
        }

        torrada?.runLoop()

        let center = NotificationCenter.default
        center.addObserver(self,
                selector: #selector(onProgress),
                name: NSNotification.Name(rawValue: TORRADA_PROGRESS), object: nil)
        center.addObserver(self, selector: #selector(onProgress),
                name: NSNotification.Name(rawValue: TORRADA_ERROR), object: nil)
        center.addObserver(self,
                selector: #selector(onFinished),
                name: NSNotification.Name(rawValue: TORRADA_FINISHED), object: nil)
        center.addObserver(self,
                selector: #selector(onRemoved),
                name: NSNotification.Name(rawValue: TORRADA_REMOVED), object: nil)
    }

    @IBAction func edit(_ sender: UIBarButtonItem?) {
        if tableView!.isEditing {
            sender?.title = "Edit"
            sender?.style = UIBarButtonItemStyle.plain
            tableView!.setEditing(false, animated: true)
        } else {
            sender?.title = "Done"
            sender?.style = UIBarButtonItemStyle.done
            tableView!.setEditing(true, animated: true)
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return downloads.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "torrentProgress", for: indexPath) as! DownloadCell
        let download = downloads[indexPath.item]

        cell.nameLabel?.text = download.descriptor.url?.absoluteString
        cell.progressView?.progress = Float(download.progress.fractionCompleted)
        cell.statusLabel?.text = download.state
        cell.onSwitchChanged = { isOn in
            if isOn {
                download.progress.resume()
            } else {
                download.progress.pause()
            }
        }

        if let error = download.error {
            cell.statusLabel?.text = error
        }

        if download.state == "seeding" {
            cell.rateLabel?.text = nil
        } else if download.progress.isPaused {
            cell.rateLabel?.text = "paused"
        } else if download.rate > 0 {
            let rate = NSString(format: "%.2f kb/s", Double(download.rate)  / 1024)
            cell.rateLabel?.text = rate as String
        }

        return cell
    }

     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {

        if editingStyle == UITableViewCellEditingStyle.delete {
            let download = downloads.remove(at: indexPath.item)
            if let torrada = torrada,
               let torrent = torrada.removeTorrent(download.descriptor),
               let filename = torrent.name {
               let _ = try? FileManager.default.removeItem(atPath: torrada.path.stringByAppendingPathComponent(filename))
                NSLog("removed \(torrent)")
            }
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.fade)
        }
    }

    func onProgress(_ notification: Notification) {
        func stateString(state: TorrentState) -> String {
            switch state {
            case .allocating: return "Allocating"
            case .checkingFiles: return "🔍 Checking files"
            case .checkingResumeData: return "🔍 Checking resume data"
            case .downloading: return "⬇️ Downloading"
            case .downloadingMetadata: return "📡 Get metadata"
            case .finished: return "✔️ Finished"
            case .seeding: return "🌱 Seeding"
            }
        }

        func matchesTorrent(_ torrent: Torrent, download: Download) -> Bool {
            if let url = torrent.url {
                return download.descriptor.url == url
            } else {
                return false
            }
        }

        if let userInfo = notification.userInfo,
           let torrent = userInfo["torrent"] as? Torrent,
           let view = tableView {

            downloads = downloads.map { download in
                if matchesTorrent(torrent, download: download) {
                    return Download(
                        descriptor: download.descriptor,
                        state: stateString(state: torrent.state),
                        progress: torrent.progress,
                        rate: torrent.progress.userInfo[ProgressUserInfoKey.throughputKey] as! Int,
                        error: torrent.error?.localizedDescription)
                } else {
                    return download
                }
            }

            if torrent.state == TorrentState.seeding {
                NSLog("WARNING: unfinished torrent: \(torrent)")
            }

            if !view.isEditing {
                view.reloadData()
            }
        }
    }

    func onFinished(_ notification: Notification) {
        if let userInfo = notification.userInfo,
           let torrent = userInfo["torrent"] as? Torrent,
           let view = tableView {
            NSLog("**** finished \(torrent)")
            view.reloadData()
        }
    }

    func onRemoved(_ notification: Notification) {
        if let userInfo = notification.userInfo,
            let infoHash = userInfo["info_hash"] as? String,
            let view = tableView {
                NSLog("**** removed torrent \(infoHash)")
                view.reloadData()
        }
    }

    @discardableResult fileprivate func clearData(_ path: String) throws -> Bool {
        let prefKey = "clear_data"
        let clearData = UserDefaults.standard.bool(forKey: prefKey)
        if clearData {
            let fileManager = FileManager.default
            for file in try fileManager.contentsOfDirectory(atPath: path) {
                try fileManager.removeItem(atPath: path.stringByAppendingPathComponent(file))
                NSLog("deleted \(file)")
            }
            UserDefaults.standard.set(false, forKey: prefKey)
            return true
        } else {
            return false
        }
    }
}

class DownloadCell: UITableViewCell {
    @IBOutlet weak var progressView: UIProgressView?
    @IBOutlet weak var statusLabel: UILabel?
    @IBOutlet weak var nameLabel: UILabel?
    @IBOutlet weak var rateLabel: UILabel?
    @IBOutlet weak var pauseSwitch: UISwitch?

    var onSwitchChanged: ((Bool) -> ())?

    @IBAction func switchChanged(_ sender: UISwitch?) {
        onSwitchChanged?(sender?.isOn ?? false)
    }
}

extension String {
    func stringByAppendingPathComponent(_ path: String) -> String {
        return (self as NSString).appendingPathComponent(path)
    }
}
